### Ruby Sinatra webapp for MD5 sum

---
#### Deployment

Bundled in docker with a v2 docker-compose file for ease of deployment:

In the downloaded direcrory:

```docker-compose up```

---
#### Clients

Can be any web browser or with curl:
```
curl -X POST http://<URL for App>:<port for app>/post -F 'file=@<full path to file here>'

# eg

curl -X POST http://localhost:8080/post -F 'file=@/home/user/Downloads/linux-4.16-rc3.tar.gz'

```

---
#### Updates

Workflow for maintaining this would github repo webhooked to docker hub automatic build.
