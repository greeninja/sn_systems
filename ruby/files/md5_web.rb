#!/usr/sbin/ruby

require "sinatra"
require "digest/md5"
require "filesize"

set :bind, '0.0.0.0'
set :port, '8080'

get '/' do
  erb :index
end

post '/post' do
  begin_time = Time.now
  s = "/tmp/#{params[:file][:filename]}"
  tmp = File.read(params[:file][:tempfile])
  f = File.open("#{s}", 'w') { |file| file.write(tmp) }
  md5 = Digest::MD5.hexdigest(File.read(s))
  end_time = Time.now
  time = (end_time - begin_time)*1000
  fs = File.size(s)
  fs_pretty = Filesize.from("#{fs} B").pretty
  erb :results, :locals =>{ :md5sum => md5, :time_taken => time, :file_size => fs_pretty }
end
