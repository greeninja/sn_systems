package main

import "net/http"
import "html/template"
import "path/filepath"
import "log"
import "os"
import "time"
import "io"
import "crypto/md5"
import "encoding/hex"
import "fmt"

func main() {
	http.HandleFunc("/", serveIndexTemplate)
	http.HandleFunc("/post", getMD5file)
	log.Println("Listening...")
	http.ListenAndServe(":8080", nil)
}

func serveIndexTemplate(w http.ResponseWriter, r *http.Request) {
	log.Println(r.Method, "/")
	lp := filepath.Join("templates", "index.html")
	tmpl, _ := template.ParseFiles(lp)
	tmpl.Execute(w, "File MD5 Sum")
}

func getMD5file(w http.ResponseWriter, r *http.Request) {
	log.Println(r.Method, "/post")
	var MD5sum string
	// Build map for template
	var values map[string]string
	values = make(map[string]string)
	// time function
	start := time.Now()
	if r.Method == "GET" {
		lp := filepath.Join("templates", "index.html")
		tmpl, _ := template.ParseFiles(lp)
		tmpl.Execute(w, "File MD5 Sum")
	} else {
		lp := filepath.Join("templates", "results.html")
		r.ParseMultipartForm(32 << 20)
		file, handler, err := r.FormFile("file")
		if err != nil {
			log.Println(err)
		}
		defer file.Close()
		// fmt.Fprintf(w, "%v", handler.Header)
		f, err := os.OpenFile("/tmp/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			log.Println(err)
			return
		}
		defer f.Close()
		io.Copy(f, file)
		filetosumpath := filepath.Join("/tmp", handler.Filename)
		filetosum, err := os.Open(filetosumpath)
		if err != nil {
			log.Println(err)
		}
		defer filetosum.Close()
		hash := md5.New()
		if _, err := io.Copy(hash, filetosum); err != nil {
			log.Println(err)
		}
		hashInBytes := hash.Sum(nil)[:16]
		MD5sum = hex.EncodeToString(hashInBytes)
		// Get filesize
		file_size, err := filetosum.Stat()
		if err != nil {
			log.Println(err)
		}
		// Build map for template
		values["file"] = handler.Filename
		values["md5sum"] = MD5sum
		values["time"] = fmt.Sprintf("%s", time.Since(start))
		values["size"] = fmt.Sprintf("%d", file_size.Size())
		tmpl, _ := template.ParseFiles(lp)
		tmpl.Execute(w, values)
	}
}
